消息推送，从后台推送消息到前台页面。

dwr实时推送消息demo,适合初次接触dwr的初学者,直接导入工程，添加到tomcat之后，
在页面输入localhost:8080/dwr/login.jsp跳转到登录页面登录，输入的密码必须为数字，
之后作为接收服务端消息推送的key，登录成功后跳转到index.jsp页面，，，，
打开另一浏览器，使用另一账号登录之后，
在url输入localhost:8080/dwr/MyJsp.jsp,
跳转到发送消息页面，在id框输入第一次登录的密码，点send,
可以看到前次登录的浏览器alert了”哈哈哈“

1）启动时加载
com.huatech.messageremind.service.DwrScriptSessionManagerUtil.java

2）推送消息到前端
com.huatech.messageremind.service.Test.java